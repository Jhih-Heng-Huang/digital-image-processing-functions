#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include "GrayScaleFunctions.h"
#include "Tests/TestBitPlanes.hpp"

std::string GetKeyMap_()
{
	return {
		"{help|<none>|print this message}"
		"{@img|<none>|input image file name}"
		"{@outputPath|<none>|the output path}"
		};
}

bool ParseArguments(
	const cv::CommandLineParser& parser,
	std::string& imgFileName,
	std::string& outputPath)
{
	if (!parser.check() ||
		!parser.has("@img") ||
		!parser.has("@outputPath"))
	{
		parser.printMessage();
		parser.printErrors();
		return false;
	}

	if (parser.has("help"))
		parser.printMessage();
	
	imgFileName = parser.get<std::string>("@img");
	outputPath = parser.get<std::string>("@outputPath");

	return true;
}

int main(int argc, char const *argv[])
{
	std::string imgFileName, outputPath;
	cv::CommandLineParser parser(argc, argv, GetKeyMap_());
	if (!ParseArguments(parser, imgFileName, outputPath))
		return -1;
	auto srcImg = cv::imread(imgFileName);
	TestBitPlaneFunctions(srcImg, outputPath);
	return 0;
}
