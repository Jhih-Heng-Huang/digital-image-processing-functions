#include "GrayScaleFunctions.h"
#include <cmath>

cv::Mat* TransformToGrayScale(const cv::Mat& img) {
	auto dstImg = new cv::Mat(img.size(), img.type());

	for (int row = 0; row < dstImg->rows; ++row)
		for (int col = 0; col < dstImg->cols; ++col) {
			auto color = img.at<cv::Vec3b>(row, col);
			auto gray = (int) (color[0] * 0.3 +
				color[1] * 0.3 + color[2] * 0.4);
			dstImg->at<cv::Vec3b>(row, col) = cv::Vec3b(gray, gray, gray);
				
		}

	return dstImg;
}

cv::Mat* TransformToBitPlanes(const cv::Mat& img, size_t bitPosition)
{
	auto dstImg = TransformToGrayScale(img);
	for (int row = 0; row < dstImg->rows; ++row)
		for (int col = 0; col < dstImg->cols; ++col)
		{
			auto color = dstImg->at<cv::Vec3b>(row, col)[0];
			auto newColor = ((color >> bitPosition) % 2) * 255;
			dstImg->at<cv::Vec3b>(row, col) = cv::Vec3b(newColor, newColor, newColor);
		}
	return dstImg;
}