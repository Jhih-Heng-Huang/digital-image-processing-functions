#pragma once
#include "GrayScaleFunctions.h"

bool TestBitPlaneFunctions(
    const cv::Mat& img,
    const std::string& outputPath)
{
    size_t bitPositions[8] = {0, 1, 2, 3, 4, 5, 6, 7};

    for (auto bitPosition: bitPositions) {
        auto dstImg = TransformToBitPlanes(img, bitPosition);
        std::string fileName = "result-" + std::to_string(bitPosition) + ".jpg";
        cv::imwrite(outputPath + fileName, *dstImg);
        delete dstImg;
    }
    return true;
}