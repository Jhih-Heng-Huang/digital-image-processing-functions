#pragma once
#include <opencv2/core.hpp>

cv::Mat* TransformToGrayScale(const cv::Mat& img);

cv::Mat* TransformToBitPlanes(const cv::Mat& img, size_t floorSize);